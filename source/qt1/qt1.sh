#!/bin/sh
if [ -d /usr/lib64/qt1 ]; then
  QT1DIR=/usr/lib64/qt1
else
  for qtd in /usr/lib64/qt1-* ; do
    if [ -d $qtd ]; then
      QT1DIR=$qtd
    fi
  done
fi
PATH="$PATH:$QT1DIR/bin"
export QT1DIR
