#!/bin/csh
if ( ! $?QT1DIR ) then
    if ( -d /usr/lib64/qt1 ) then
        setenv QT1DIR /usr/lib64/qt1
    else
        foreach qtd ( /usr/lib64/qt1-* )
            if ( -d $qtd ) then
                setenv QT1DIR $qtd
            endif
        end
    endif
endif
set path = ( $path $QT1DIR/bin )
