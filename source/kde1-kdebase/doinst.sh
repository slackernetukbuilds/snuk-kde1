if [ -d usr/share/icons/kde1 ]; then
  if [ -x /usr/bin/gtk-update-icon-cache ]; then
    /usr/bin/gtk-update-icon-cache -f usr/share/icons/kde1 >/dev/null 2>&1
  fi
fi
