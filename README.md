# This project based on [KDE-Historical](https://invent.kde.org/historical) &  [Slackware](http://www.slackware.com/)

# Installing


**Building Instructions**


```
$ git clone https://gitlab.com/slackernetukbuilds/snuk-kde1.git 
$ cd snuk-kde1
$ sudo sh build
```

**Configuration**

Add to /etc/slackpkg/blacklist file

`[0-9]+_snuk`

Next, we're need to relogin, to be sure all commands variables are on place.

As user `xwmconfig` to setup kde1 as standard-session


**ScreenShot**

![kde1-gitlab](/uploads/0d2e66ca7de66a1bb55ec2e5ea1d7ebb/kde1-gitlab.png)
